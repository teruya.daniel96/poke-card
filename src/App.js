import logo from './logo.svg';
import './App.css';
import PaginaPrincipal from "./components/PaginaPrincipal";

function App() {

  return (
    <div className="App">
        <PaginaPrincipal />
    </div>
  );
}

export default App;
